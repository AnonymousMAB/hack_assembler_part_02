#include <iostream>
using namespace std;
#include <ctime>
#include <string>
#include <cmath>
#include <fstream>

bool check_if_A_instruct(string line)// checks if instruction is an A instruction
{
 for (int counter = 0; line[counter]!='\0'; counter++)
 {
     if (line[counter] =='@')
         { return true; }
 }

 return false;
}

bool is_number(string line) // tells if string consists entirely of numbers
{
    int temp;
    int check = 0;
 for (int counter = 0; counter < line.size(); counter++)
 { temp = line[counter];
   if (!((temp >= 65 && temp <= 90) || (temp >= 97 && temp <= 122)))
   {
      check++;
   }
 }
 if (check  == line.size())
     {return true;}
     
 return false;
}

bool is_commented(string line, int pos) // pos is current index of array
{                                       // returns true if there exists comments from current array index 
 if( (pos + 1) < line.size())
 {  
  if (line[pos] =='/' && line[pos + 1] == '/')
  {
    return true;
  }
 } 

 return false;
}

bool is_comment_valid(string line, int pos) // tells us if comment is valid 
                                            // pos is starting position of '/'
{ 
  int index = pos;  
  if (line[pos + 1] == '\0')
      { return false;}

  else if(line[pos + 1] == '/')
      { return true;}
  
  index++;  

   while (line[index] != '/' && line[index] != '\0')
   {
       if (line[index] != ' ')
           { return false;}
        
        index++;   
   }  

  return true;  
}

string isolate_label(string line, int index) // isolates label_name from label
{
  string label = "";
  index++;
  for (int inner = index; line[inner] != ')' && !is_commented(line, inner); inner++)
            {
                if(line[inner] != ' ' && line[inner] != '/')
                {
                    label = label + line[inner];
                }
            }

    return label;        
}


bool check_eq_char_exists(string line, int pos) // checks if "=" exists in a line
{

  for (int counter = pos; line[counter] !='\0'; counter++)
  {
   if (line[counter] == '=')
       { return true; }
  }

  return false;
}

bool are_same(int a, int b)
{
    if (a == b)
        { return true; }
    else
        { return false;}
        
}
bool check_semi_col_exists(const string line, int pos) // checks if ';' exists in a line
{

  for (int counter = pos; line[counter] !='\0'; counter++)
  {
   if (line[counter] == ';')
       { return true; }
  }

  return false;
}
void dec_to_bin(string num) // converts a decimal number to its binary equivalent
{                        // Note :: Number has to be less than or equal to 32767 and greater than or equal to 0
                        // stoi converts string to integer
    char out[17]; 
    int copy = stoi(num);

    for (int counter = 0; counter <= 15; counter++)
    {
        out[counter] = '0';
    }
    

    for (int counter = 0; copy != 0; counter++)
    {    
        if (copy == 1)
        {
            out[0] = '1';
        }

         if (pow(2, counter) > copy)
         {
             copy = copy - pow(2, counter - 1);
             out[counter - 1] = '1';
             counter = 0;
         }
    }

    for (int counter = 15; counter >= 0; counter--)
    {
        cout << out[counter];
        
    }

}

void dec_to_bin2(string num, ofstream &write) // converts a decimal number to its binary equivalent
{                        // Note :: Number has to be less than or equal to 32767 and greater than or equal to 0
    char out[17];        // outputs result in a file
    int copy = stoi(num); // stoi converts string to integer

    for (int counter = 0; counter <= 15; counter++)
    {
        out[counter] = '0';
    }
    

    for (int counter = 0; copy != 0; counter++)
    {    
        if (copy == 1)
        {
            out[0] = '1';
        }

         if (pow(2, counter) > copy)
         {
             copy = copy - pow(2, counter - 1);
             out[counter - 1] = '1';
             counter = 0;
         }
    }

    for (int counter = 15; counter >= 0; counter--)
    {
       write << out[counter];
        
    }

}

bool is_line_empty(string line) // checks if line is empty or not
{
  for(int counter = 0; counter < line.size(); counter++)
  {
      if (line[counter] != ' ')
      {
          return false;
      }
  }

  return true;
}


bool should_ignore_line(string line) // tells us if we should ignore a line or not 
{
  for (int counter = 0; counter < line.size(); counter++)
  {
      if (line[counter] == '/')
          {
              if (!is_comment_valid(line, counter))
              {
                  return false;
              }
          }
                 
  }

  return true;
}

bool does_overflow_occur(string num) // tells us if an overflow occurs if value is greater than 32767 or value is less than 0
{                                    // stoi converts string to integer
    int copy = stoi(num);
    
    if (copy < 0 || copy > 32767)
    {
        return true;
    }

   return false; 
}